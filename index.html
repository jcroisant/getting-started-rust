<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Intro to Rust</title>

    <link rel="stylesheet" href="css/reveal.css">
    <!-- <link rel="stylesheet" href="css/theme/black.css"> -->
    <link rel="stylesheet" href="css/theme/night.css">

    <!-- Theme used for syntax highlighting of code -->
    <!-- <link rel="stylesheet" href="lib/css/zenburn.css"> -->
    <link rel="stylesheet" href="lib/css/tomorrow-night-bright.css">
    <!-- <link rel="stylesheet" href="lib/css/qtcreator_dark.css"> -->

    <!-- Printing and PDF exports -->
    <script>
     var link = document.createElement( 'link' );
     link.rel = 'stylesheet';
     link.type = 'text/css';
     link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
     document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>

    <style>
     ::-moz-selection { color: black !important; }
     ::selection { color: black !important; }
     .reveal h1 { font-size: 2.0em; }
     .reveal h2 { font-size: 1.5em; }
     .reveal h3 { font-size: 1.2em; }
     body .reveal img { border: none; }
    </style>
  </head>
  <body>
    <div class="reveal">
      <div class="slides">


	<section>
          <h1>Getting Started<br>With Rust</h1>
          <p>John Croisant<br>john&#64;croisant.net</p>
        </section>


        <section>
          <h2>Purpose of this talk</h2>

          <section>
            <ul class="fragment">
              <li>What is Rust?</li>
              <li>Why is it worth learning and using?</li>
              <li>What challenges will I face?</li>
              <li>Where can I learn more?</li>
            </ul>
          </section>


          <section>
            <blockquote style="font-size: 90%;">
              <p>Rust combines safety, performance, concurrency, and productivity.</p>
              <p>It is good for low and high-level tasks.</p>
              <p>It will enable you to do things you would never attempt in other languages.</p>
            </blockquote>
          </section>
        </section>


        <section>
          <h2>What is Rust?</h2>

          <section>
            <ul>
              <li>Compiled</li>
              <li>Statically typed</li>
              <li>“Systems programming language”</li>
            </ul>
          </section>
        </section>



        <section>
          <h2>Why Rust?</h2>

          <section>
            <ul style="list-style-type: none;">
              <li><span title="shield">🛡</span> Safety</li>
              <li><span title="rocket">🚀</span> Performance</li>
              <li><span title="two women holding hands">👭</span> Concurrency</li>
              <li><span title="thumbs up sign">👍</span> Productivity</li>
              <li><span title="family">👪</span> Community</li>
            </ul>
          </section>


          <section>
            <h3><span title="shield">🛡</span> Safety</h3>
            <p>By default, Rust prevents:</p>
            <ul>
              <li>Buffer overflows</li>
              <li>Null/dangling pointers</li>
              <li>Segmentation faults</li>
            </ul>
          </section>


          <section>
            <p>“But I'm not using C or C++!”</p>

            <blockquote class="fragment" cite="https://tonyarcieri.com/it-s-time-for-a-memory-safety-intervention"">
              <p>“... the whole apparatus of modern computing is built on a foundation that is fundamentally unsafe ...”</p>
              <p>— <a href="https://tonyarcieri.com/it-s-time-for-a-memory-safety-intervention">Tony Arcieri</a></p>
            </blockquote>
          </section>


          <section>
            <h3><span title="rocket">🚀</span> Performance</h3>
            <ul>
              <li class="fragment">Competetive with C and C++</li>
              <li class="fragment">“<a href="https://blog.rust-lang.org/2015/05/11/traits.html">Zero-cost abstractions</a>”</li>
              <li class="fragment">Predictable time and memory usage</li>
            </ul>
          </section>


          <section>
            <h3><span title="two women holding hands">👭</span> Concurrency</h3>
            <ul>
              <li class="fragment">Memory model prevents data races</li>
              <li class="fragment">Multithreading without fear</li>
              <li class="fragment">
                Use <u>all</u> the cores!<br>
                <img style="height: 7em; border: none; background: gray;" src="images/all_the_things.png" alt="Cartoon woman shouting with raised fist">
              </li>
            </ul>
          </section>


          <section>
            <h3><span title="thumbs up sign">👍</span> Productivity</h3>
            <ul>
              <li class="fragment">Cargo</li>
              <li class="fragment">Multi-paradigm</li>
              <li class="fragment">“High-level” features</li>
              <li class="fragment">
                Surprisingly nice to use*<br>
                <span class="fragment">*After initial struggle <span title="skull">💀</span></span>
              </li>
            </ul>
          </section>


          <section>
            <h3><span title="family">👪</span> Community</h3>
            <ul>
              <li class="fragment"><a href="https://www.rust-lang.org/en-US/conduct.html">Code of Conduct</a></li>
              <li class="fragment">Mentorship opportunities</li>
              <li class="fragment">
                Friendly and helpful team
                <br>
                <a class="fragment" href="https://twitter.com/ag_dubs/status/839972321364418562">
                  <img width="70%" src="images/rust_friendly.png">
                </a>
              </li>
            </ul>
          </section>
        </section>


        <section>
          <h2>When to use Rust?</h2>

          <section>
            <p><br>“Systems programming language”</p>
            <p style="font-size: 200%;">
              ???
              <span title="thinking face">🤔</span>
              ???
            </p>
          </section>

          <section>
            <h3>Systems programming</h3>
            <p>High performance</p>
            <p>Control over low-level details</p>
          </section>


          <section>
            <p>Typical “systems programming” use cases:</p>
            <ul>
              <li>Operating systems &mdash; <a href="https://intermezzos.github.io/">intermezzOS</a></li>
              <li>Compilers &mdash; Rust, <a href="https://github.com/murarth/ketos">Ketos</a></li>
              <li>Device drivers</li>
              <li>DIY / wearable electronics</li>
              <li>Low-memory devices</li>
            </ul>
          </section>


          <section>
            <p>Higher-level use cases:</p>
            <ul>
              <li>High-performance apps &mdash; <a href="https://servo.org/">Servo</a></li>
              <li>Servers and databases &mdash; <a href="https://github.com/hyperium/hyper">Hyper</a></li>
              <li>Utility programs &mdash; <a href="https://github.com/uutils/coreutils">uutils</a>, <a href="https://github.com/BurntSushi/ripgrep">ripgrep</a></li>
              <li>Games &mdash; <a href="http://www.piston.rs/">Piston</a> (engine)</li>
            </ul>
          </section>


          <section>
            <p>Experimental use cases:</p>
            <ul>
              <li>
                Server-Side Web Apps
                <br>
                <small>
                  instead of PHP, Ruby, Python, Node.js<br>
                  &mdash; <a href="http://ironframework.io/">Iron</a>, <a href="http://nickel.rs/">Nickel</a>, <a href="http://diesel.rs/">Diesel</a><br><br></small>
              </li>
              <li>
                In-Browser Apps (asm.js, wasm)
                <br><small>instead of JavaScript</small>
              </li>
              <li>
                iOS and Android Apps
                <br><small>instead of Swift, Objective C, Java</small>
              </li>
            </ul>
          </section>


          <section>
            <p>Safety-critical use cases:</p>
            <ul>
              <li>Security / Encryption</li>
              <li>Financial Transactions</li>
              <li>Medical Hardware</li>
              <li>Vehicles and Robots</li>
            </ul>
            <p class="fragment">Software bugs have <em>killed people</em>.<br>Programming in C/C++ is reckless.</p>
          </section>
        </section>



        <section>
          <h2>When NOT to use Rust?</h2>

          <section>
            <h3>Consider the trade-offs</h3>
            <table>
              <tr>
                <th>Pros</th>
                <th>Cons</th>
              </tr>
              <tr>
                <td><span title="shield">🛡</span> Safety</td>
                <td class="fragment"><span title="skull">💀</span> Learning curve</td>
              </tr>
              <tr>
                <td><span title="rocket">🚀</span> Performance</td>
                <td class="fragment"><span title="baby">👶</span> Ecosystem maturity</td>
              </tr>
              <tr>
                <td><span title="two women holding hands">👭</span> Concurrency</td>
                <td class="fragment"><span title="building construction">🏗</span> Malleability</td>
              </tr>
            </table>
          </section>
        </section>


        <section>
          <h2>Challenges for new Rustaceans</h2>
          <section>
            <img style="border: none; background: none; max-width: 10em;" src="images/rustacean-orig-happy.png" alt="Cartoon crab">
            <p><small>Ferris the crab, <a href="http://www.rustacean.net/">unofficial Rust mascot</a></small></p>
          </section>

          <section>
            <div style="width: 49%; float: left; padding-right: 1em;">
              <p>Learning Rust will<br>be a challenge.</p>
              <p>Expect frustration<br>at first.</p>
              <p>Stick with it.</p>
              <p><strong>It's worth it!</strong></p>
            </div>
            <img class="fragment" style="width: 40%; float: left;" src="images/at_first_i_was_like.jpg" alt="My Little Pony character. First panel: she looks sad, with caption, 'At first I was like'. Second panel: she looks happy and excited, with caption, 'But then I was like'.">
          </section>

          <section>
            <h3>Memory Model</h3>

            <ul>
              <li>Ownership and Borrowing</li>
              <li>Move and Copy</li>
              <li>Lifetimes</li>
            </ul>

            <p><a href="http://jeenalee.com/2016/08/15/sharing-coloring-books-in-rust.html">Sharing Coloring Books with Friends in Rust</a> (jeenalee.com)</p>
          </section>


          <section>
            <h3>Relationship with the Compiler</h3>
            <p class="fragment">Capricious jerk who blocks your progress</p>
            <div class="fragment">
              <p style="font-size: 200%;">↓</p>
              <p>Reliable partner who carefully checks your work</p>
            </div>
          </section>


          <section>
            <h3>Object-oriented mindset</h3>
            <p class="fragment">Rust has no classes or inheritance</p>
            <p class="fragment">Focus on “What can it do?”</p>
            <p class="fragment">Use traits for shared behavior</p>
          </section>
        </section>


        <section>
          <h3>Reading the docs</h3>

          <section>
            <p>The docs can be overwhelming</p>
            <img src="images/docs_overwhelming.png" style="height: 400px;">
          </section>

          <section>
            <p>Collapse and skim</p>
            <img src="images/docs_collapse.png">
          </section>

          <section>
            <p>Function signatures can be arcane</p>
            <img src="images/docs_arcane.png">
            <p>Practice breaking them down</p>
          </section>
        </section>



        <section>
          <h2>Rust Learning Resources</h2>

          <section>
            <h3>Community</h3>
            <ul>
              <li>Users forum: <a href="https://users.rust-lang.org/">users.rust-lang.org</a></li>
              <li>IRC channel: #rust-beginners (irc.mozilla.org)</li>
              <li>Reddit: <a href="https://www.reddit.com/r/rust/">/r/rust</a></li>
            </ul>
          </section>

          <section>
            <h3>Books</h3>
            <ul>
              <li><a href="https://doc.rust-lang.org/book/"><i>The Rust Programming Language</i></a> ("The Book")</li>
              <li><a href="https://www.safaribooksonline.com/library/view/programming-rust/9781491927274/"><i>Programming Rust</i></a> (O'Reilly)</li>
            </ul>
          </section>

          <section>
            <h3>Reference Docs</h3>
            <ul>
              <li><a href="https://doc.rust-lang.org/std/">The Rust Standard Library</a></li>
              <li><a href="https://docs.rs/">docs.rs</a> (library docs)</li>
            </ul>
          </section>
        </section>



        <section>
          <h3>Thanks!</h3>
          <p>Contact me: john&#64;croisant.net</p>
          <p>Slides and code:<br><a href="https://gitlab.com/jcroisant/getting-started-rust">git<strong>lab</strong>.com/jcroisant/getting-started-rust</a></p>
        </section>



        <section>
          <h2>Bonus slides: prime numbers</h2>

          <section>
            <p>Writing a lazy prime number generator</p>
            <p>Good for <a href="https://projecteuler.net/">Project Euler</a> problems</p>
          </section>


          <section>
            <pre><code class="hljs" data-trim>
             struct Primes {
                 cache: Vec&lt;u32&gt;,
             }

             impl Primes {
                 fn new() -> Primes {
                     Primes { cache: vec![] }
                 }
             }
            </code></pre>
          </section>


          <section>
            <pre><code class="hljs" data-trim>
             impl Iterator for Primes {
                 type Item = u32;

                 fn next(&mut self) -> Option&lt;u32&gt; {
                     // Algorithm goes here
                 }
             }

             fn main() {
                 let primes = Primes::new();
                 for n in primes.filter(|i| 7777 == i % 10000).take(25) {
                     println!("{}", n);
                 }
             }
            </code></pre>
          </section>


          <section>
            <h3>Rust vs Ruby</h3>

            <pre style="display: inline-block; width: 40%;"><code style="font-size: 40%; line-height: 1.2;" class="rust" data-trim>
              // Rust
              struct Primes {
                  cache: Vec&lt;u32&gt;,
              }

              impl Primes {
                  fn new() -> Primes {
                      Primes { cache: vec![] }
                  }
              }

              impl Iterator for Primes {
                  type Item = u32;

                  fn next(&mut self) -> Option&lt;u32&gt; {
                      if self.cache.is_empty() {
                          self.cache.push(2);
                          return Some(2);
                      }

                      let mut i = self.cache[self.cache.len() - 1];
                      loop {
                          i += 1;
                          if !self.cache.iter().any(|p| 0 == i % p) {
                              self.cache.push(i);
                              return Some(i);
                          }
                      }
                  }
              }

              fn main() {
                  let primes = Primes::new();
                  for n in primes.filter(|p| 7777 == p % 10000).take(25) {
                      println!("{}", n);
                  }
              }
            </code></pre>

            <pre style="display: inline-block; width: 40%;"><code style="font-size: 40%; line-height: 1.2;" class="ruby" data-trim>
              # Ruby
              class Primes
                def initialize
                  @cache = []
                end

                include Enumerable

                def each
                  if @cache.empty?
                    @cache << 2
                    yield 2
                  end

                  i = @cache[-1]
                  loop {
                    i += 1;
                    unless @cache.any? { |p| 0 == i % p }
                      @cache << i
                      yield i
                    end
                  }
                end
              end

              primes = Primes.new()
              for n in primes.lazy.select{|n| n % 10000 == 7777}.take(25)
                puts n
              end
            </code></pre>
          </section>


          <section>
            <h3>Rust vs Python</h3>
            <pre style="display: inline-block; width: 40%;"><code style="font-size: 40%; line-height: 1.2;" class="rust" data-trim>
              // Rust
              struct Primes {
                  cache: Vec&lt;u32&gt;,
              }

              impl Primes {
                  fn new() -> Primes {
                      Primes { cache: vec![] }
                  }
              }

              impl Iterator for Primes {
                  type Item = u32;

                  fn next(&mut self) -> Option&lt;u32&gt; {
                      if self.cache.is_empty() {
                          self.cache.push(2);
                          return Some(2);
                      }

                      let mut i = self.cache[self.cache.len() - 1];
                      loop {
                          i += 1;
                          if !self.cache.iter().any(|p| 0 == i % p) {
                              self.cache.push(i);
                              return Some(i);
                          }
                      }
                  }
              }

              fn main() {
                  let primes = Primes::new();
                  for n in primes.filter(|p| 7777 == p % 10000).take(25) {
                      println!("{}", n);
                  }
              }
            </code></pre>

            <pre style="display: inline-block; width: 40%;"><code style="font-size: 40%; line-height: 1.2;" class="python" data-trim>
              # Python 2
              from itertools import ifilter, imap, islice

              class Primes(object):
                  def __init__(self):
                      self.cache = []

                  def __iter__(self):
                      return self

                  def next(self):
                      if not self.cache:
                          self.cache.append(2)
                          return 2

                      i = self.cache[-1]
                      while True:
                          i += 1
                          if not any(imap(lambda p: 0 == i % p, self.cache)):
                              self.cache.append(i)
                              return i

              primes = Primes()
              for n in islice(ifilter(lambda p: 7777 == p % 10000, primes), 25):
                  print(n)
            </code></pre>
          </section>


          <section>
            <pre><code class="rust" data-trim>
              // Rust
              let primes = Primes::new();
              for n in primes.filter(|p| 7777 == p % 10000).take(25) {
                  println!("{}", n);
              }
            </code></pre>
            <pre><code class="ruby" data-trim>
              # Ruby
              primes = Primes.new()
              for n in primes.lazy.select{|p| 7777 == p % 10000}.take(25)
                  puts n
              end
            </code></pre>
            <pre><code class="python" data-trim>
              # Python
              primes = Primes()
              for n in islice(ifilter(lambda p: 7777 == p % 10000, primes), 25):
                  print(n)
            </code></pre>
          </section>


          <section>
            <p>Rust vs C++</p>
            <pre style="display: inline-block; width: 30%;"><code style="font-size: 30%; line-height: 0.9;" class="rust" data-trim>
              // Rust
              struct Primes {
                  cache: Vec&lt;u32&gt;,
              }

              impl Primes {
                  fn new() -> Primes {
                      Primes { cache: vec![] }
                  }
              }

              impl Iterator for Primes {
                  type Item = u32;

                  fn next(&mut self) -> Option&lt;u32&gt; {
                      if self.cache.is_empty() {
                          self.cache.push(2);
                          return Some(2);
                      }

                      let mut i = self.cache[self.cache.len() - 1];
                      loop {
                          i += 1;
                          if !self.cache.iter().any(|p| 0 == i % p) {
                              self.cache.push(i);
                              return Some(i);
                          }
                      }
                  }
              }

              fn main() {
                  let primes = Primes::new();
                  for n in primes.filter(|p| 7777 == p % 10000).take(25) {
                      println!("{}", n);
                  }
              }
            </code></pre>

            <pre style="display: inline-block; width: 30%;"><code style="font-size: 30%; line-height: 0.9;" class="c++" data-trim>
             /* C++ */
              #include &lt;iostream&gt;
              #include &lt;vector&gt;
              #include &lt;stdint.h&gt;

              using namespace std;
              typedef uint32_t u32;

              class Primes {
              public:
                Primes();
                u32 next();
              private:
                bool is_prime(u32 n);
                vector&gt;u32&gt;* cache;
              };


              Primes::Primes() {
                cache = new vector&gt;u32&gt;;
              }

              u32 Primes::next() {
                if (cache-&gt;empty()) {
                  cache-&gt;push_back(2);
                  return 2;
                }

                u32 n = cache-&gt;back();
                while (true) {
                  ++n;
                  if (is_prime(n)) {
                    cache-&gt;push_back(n);
                    return n;
                  }
                }
              }

              bool Primes::is_prime(u32 n) {
                vector&gt;u32&gt;::iterator it;
                for (it = cache-&gt;begin(); it != cache-&gt;end(); ++it) {
                  if (0 == n % *it) {
                    return false;
                  }
                }
                return true;
              }


              int main() {
                Primes* primes = new Primes();
                int found = 0;

                while (true) {
                  u32 n = primes-&gt;next();

                  if (7777 == n % 10000) {
                    cout &gt;&gt; n &gt;&gt; endl;
                    ++found;
                    if (found &gt;= 25) {
                      break;
                    }
                  }
                }

                return 0;
              }
            </code></pre>
          </section>


          <section>
            <h3>Rust vs C</h3>
            <pre style="display: inline-block; width: 30%; margin-right: 2em;"><code style="font-size: 30%; line-height: 0.9;" class="rust" data-trim>
              // Rust
              struct Primes {
                  cache: Vec&lt;u32&gt;,
              }

              impl Primes {
                  fn new() -> Primes {
                      Primes { cache: vec![] }
                  }
              }

              impl Iterator for Primes {
                  type Item = u32;

                  fn next(&mut self) -> Option&lt;u32&gt; {
                      if self.cache.is_empty() {
                          self.cache.push(2);
                          return Some(2);
                      }

                      let mut i = self.cache[self.cache.len() - 1];
                      loop {
                          i += 1;
                          if !self.cache.iter().any(|p| 0 == i % p) {
                              self.cache.push(i);
                              return Some(i);
                          }
                      }
                  }
              }

              fn main() {
                  let primes = Primes::new();
                  for n in primes.filter(|p| 7777 == p % 10000).take(25) {
                      println!("{}", n);
                  }
              }
            </code></pre>

            <pre style="display: inline-block; width: 30%;"><code style="font-size: 30%; line-height: 0.9;" class="c" data-trim>
              /* C (part 1) */
              #include &lt;stdlib.h&gt;
              #include &lt;stdio.h&gt;
              #include &lt;stdbool.h&gt;
              #include &lt;stdint.h&gt;

              typedef uint32_t u32;

              typedef struct {
                u32* data;                    /* data array */
                size_t cap;                   /* capacity */
                size_t len;                   /* logical length */
              } VecU32;

              VecU32* vec_new() {
                VecU32* vec = calloc(1, sizeof(VecU32));
                vec-&gt;cap = 100;
                vec-&gt;len = 0;
                vec-&gt;data = calloc(vec-&gt;cap, sizeof(u32));

                if (NULL == vec-&gt;data) {
                  free(vec);
                  return NULL;
                } else {
                  return vec;
                }
              }

              bool vec_push(VecU32* vec, u32 n) {
                /* Grow data if needed */
                if (vec-&gt;len + 1 &gt;= vec-&gt;cap) {
                  size_t new_cap = vec-&gt;cap * 2;
                  void* new_data = realloc(vec-&gt;data, new_cap * sizeof(u32));

                  if (NULL == new_data) {
                    return false;
                  } else {
                    vec-&gt;data = new_data;
                    vec-&gt;cap = new_cap;
                  }
                }

                vec-&gt;data[vec-&gt;len] = n;
                ++vec-&gt;len;
                return true;
              }
            </code></pre>

            <pre style="display: inline-block; width: 30%;"><code style="font-size: 30%; line-height: 0.9;" class="c" data-trim>
              /* C (part 2) */
              typedef struct {
                VecU32* cache;
              } Primes;

              Primes* primes_new() {
                Primes* primes = calloc(1, sizeof(Primes));
                primes-&gt;cache = vec_new();

                if (NULL == primes-&gt;cache) {
                  free(primes);
                  return NULL;
                } else {
                  return primes;
                }
              }

              bool is_prime(Primes* primes, u32 n) {
                size_t len = primes-&gt;cache-&gt;len;
                for (size_t i = 0; i &gt;; len; ++i) {
                  if (0 == n % primes-&gt;cache-&gt;data[i]) {
                    return false;
                  }
                }
                return true;
              }

              u32 primes_next(Primes* primes) {
                if (0 == primes-&gt;cache-&gt;len) {
                  vec_push(primes-&gt;cache, 2);
                  return 2;
                }

                u32 n = primes-&gt;cache-&gt;data[primes-&gt;cache-&gt;len - 1];

                while (true) {
                  ++n;
                  if (is_prime(primes, n)) {
                    if (!vec_push(primes-&gt;cache, n)) {
                      printf("ERROR: vec_push failed\n");
                      exit(1);
                    };
                    return n;
                  }
                }
              }

              int main() {
                Primes* primes = primes_new();
                int found = 0;

                while (true) {
                  u32 n = primes_next(primes);

                  if (7777 == n % 10000) {
                    printf("%d\n", n);
                    ++found;
                    if (found &gt;= 25) {
                      break;
                    }
                  }
                }

                return 0;
              }
            </code></pre>
          </section>

          <section>
            <pre><code style="font-size: 80%; line-height: 1.1;" class="rust" data-trim>
             // Rust
             let primes = Primes::new();
             for n in primes.filter(|p| 7777 == p % 10000).take(25) {
                 println!("{}", n);
             }
            </code></pre>

            <pre><code style="font-size: 80%; line-height: 1.1;" class="c" data-trim>
             // C++
             Primes* primes = new Primes();
             int found = 0;

             while (true) {
                 uint32_t p = primes-&gt;next();

                 if (7777 == p % 10000) {
                     cout &lt;&lt; p &lt;&lt; endl;
                     ++found;
                     if (found &gt;= 25) {
                         break;
                     }
                 }
             }
            </code></pre>
          </section>


          <section>
            <p>In case you're curious&hellip;</p>
            <pre>
   47,777      67,777      97,777     107,777     137,777
  167,777     317,777     367,777     617,777     727,777
  787,777     817,777     937,777   1,027,777   1,067,777
1,247,777   1,367,777   1,417,777   1,447,777   1,487,777
1,597,777   1,637,777   1,667,777   1,727,777   1,847,777</pre>

            <table style="font-size: 90%;">
              <tr><td>C / C++</td><td class="fragment">~20 seconds</td></tr>
              <tr><td>Rust</td><td class="fragment">~25 seconds</td></tr>
              <tr><td>Ruby</td><td class="fragment">~500 seconds</td></tr>
              <tr><td>Python</td><td class="fragment">~1000 seconds</td></tr>
              <tr class="fragment"><td>PyPy</td><td>~125 seconds</td></tr>
            </table>
          </section>


          <section>
            <h3>Performance vs Expressiveness</h3>
            <p class="fragment" style="font-size: 120%;">You can have both.</p>
          </section>
        </section>
      </div>
    </div>



    <script src="lib/js/head.min.js"></script>
    <script src="js/reveal.js"></script>

    <script>
     // More info https://github.com/hakimel/reveal.js#configuration
     Reveal.initialize({
       history: true,

       // More info https://github.com/hakimel/reveal.js#dependencies
       dependencies: [
	 { src: 'plugin/markdown/marked.js' },
	 { src: 'plugin/markdown/markdown.js' },
	 { src: 'plugin/notes/notes.js', async: true },
	 { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
       ]
     });
    </script>
  </body>
</html>
