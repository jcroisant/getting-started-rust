// Naive algorithm for generating prime numbers.
//
// To the extent possible under law, the author (John Croisant) has
// waived all copyright and related or neighboring rights to this
// work. See: https://creativecommons.org/publicdomain/zero/1.0/

struct Primes {
    cache: Vec<u32>,
}

impl Primes {
    fn new() -> Primes {
        Primes { cache: vec![] }
    }
}

impl Iterator for Primes {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        if self.cache.is_empty() {
            self.cache.push(2);
            return Some(2);
        }

        let mut i = self.cache[self.cache.len() - 1];
        loop {
            i += 1;
            if !self.cache.iter().any(|p| 0 == i % p) {
                self.cache.push(i);
                return Some(i);
            }
        }
    }
}

fn main() {
    let primes = Primes::new();
    for n in primes.filter(|p| 7777 == p % 10000).take(25) {
        println!("{}", n);
    }
}


#[cfg(test)]
mod tests {
    use super::Primes;

    #[test]
    fn test_primes_next() {
        let mut primes = Primes::new();
        assert_eq!(2, primes.next().unwrap());
        assert_eq!(3, primes.next().unwrap());
        assert_eq!(5, primes.next().unwrap());
        assert_eq!(7, primes.next().unwrap());
    }
}
