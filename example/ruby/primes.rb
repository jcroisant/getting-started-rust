# Naive algorithm for generating prime numbers.
#
# To the extent possible under law, the author (John Croisant) has
# waived all copyright and related or neighboring rights to this work.
# See: https://creativecommons.org/publicdomain/zero/1.0/

class Primes
  def initialize
    @cache = []
  end

  include Enumerable

  def each
    if @cache.empty?
      @cache << 2
      yield 2
    end

    i = @cache[-1]
    loop {
      i += 1;
      unless @cache.any? { |p| 0 == i % p }
        @cache << i
        yield i
      end
    }
  end
end

primes = Primes.new()
for n in primes.lazy.select{|n| n % 10000 == 7777}.take(25)
  puts n
end
