This folder contains implementations in several programming languages
of a naïve algorithm for generating prime numbers. These examples are
not intended to show the most efficient way to compute prime numbers.

To the extent possible under law, the author (John Croisant) has
waived all copyright and related or neighboring rights to this work.
See: https://creativecommons.org/publicdomain/zero/1.0/
