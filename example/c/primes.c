/* Naive algorithm for generating prime numbers.
 *
 * To the extent possible under law, the author (John Croisant) has
 * waived all copyright and related or neighboring rights to this
 * work. See: https://creativecommons.org/publicdomain/zero/1.0/
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

typedef uint32_t u32;


/* Growable array of unsigned 32-bit integers */
typedef struct {
  u32* data;                    /* data array */
  size_t cap;                   /* capacity */
  size_t len;                   /* logical length */
} VecU32;


VecU32* vec_new() {
  VecU32* vec = calloc(1, sizeof(VecU32));
  vec->cap = 100;
  vec->len = 0;
  vec->data = calloc(vec->cap, sizeof(u32));

  if (NULL == vec->data) {
    free(vec);
    return NULL;
  } else {
    return vec;
  }
}


bool vec_push(VecU32* vec, u32 n) {
  /* Grow data if needed */
  if (vec->len + 1 >= vec->cap) {
    size_t new_cap = vec->cap * 2;
    void* new_data = realloc(vec->data, new_cap * sizeof(u32));

    if (NULL == new_data) {
      return false;
    } else {
      vec->data = new_data;
      vec->cap = new_cap;
    }
  }

  vec->data[vec->len] = n;
  ++vec->len;
  return true;
}



/* Generator that yields prime numbers. */
typedef struct {
  VecU32* cache;
} Primes;


Primes* primes_new() {
  Primes* primes = calloc(1, sizeof(Primes));
  primes->cache = vec_new();

  if (NULL == primes->cache) {
    free(primes);
    return NULL;
  } else {
    return primes;
  }
}


/* Return false if n is divisible by any known prime. */
bool is_prime(Primes* primes, u32 n) {
  size_t len = primes->cache->len;
  for (size_t i = 0; i < len; ++i) {
    if (0 == n % primes->cache->data[i]) {
      return false;
    }
  }
  return true;
}


u32 primes_next(Primes* primes) {
  if (0 == primes->cache->len) {
    vec_push(primes->cache, 2);
    return 2;
  }

  u32 n = primes->cache->data[primes->cache->len - 1];

  while (true) {
    ++n;
    if (is_prime(primes, n)) {
      if (!vec_push(primes->cache, n)) {
        printf("ERROR: vec_push failed\n");
        exit(1);
      };
      return n;
    }
  }
}



int main() {
  Primes* primes = primes_new();
  int found = 0;

  while (true) {
    u32 n = primes_next(primes);

    if (7777 == n % 10000) {
      printf("%d\n", n);
      ++found;
      if (found >= 25) {
        break;
      }
    }
  }

  return 0;
}
