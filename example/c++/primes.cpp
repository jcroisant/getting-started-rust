// Naive algorithm for generating prime numbers.
//
// To the extent possible under law, the author (John Croisant) has
// waived all copyright and related or neighboring rights to this
// work. See: https://creativecommons.org/publicdomain/zero/1.0/

#include <iostream>
#include <vector>
#include <stdint.h>

using namespace std;
typedef uint32_t u32;

// Generator that yields prime numbers.
class Primes {
public:
  Primes();
  u32 next();
private:
  bool is_prime(u32 n);
  vector<u32>* cache;
};


Primes::Primes() {
  cache = new vector<u32>;
}

u32 Primes::next() {
  if (cache->empty()) {
    cache->push_back(2);
    return 2;
  }

  u32 n = cache->back();
  while (true) {
    ++n;
    if (is_prime(n)) {
      cache->push_back(n);
      return n;
    }
  }
}

// Return false if n is divisible by any known prime.
bool Primes::is_prime(u32 n) {
  vector<u32>::iterator it;
  for (it = cache->begin(); it != cache->end(); ++it) {
    if (0 == n % *it) {
      return false;
    }
  }
  return true;
}


int main() {
  Primes* primes = new Primes();
  int found = 0;

  while (true) {
    u32 n = primes->next();

    if (7777 == n % 10000) {
      cout << n << endl;
      ++found;
      if (found >= 25) {
        break;
      }
    }
  }

  return 0;
}
