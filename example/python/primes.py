# Naive algorithm for generating prime numbers.
#
# To the extent possible under law, the author (John Croisant) has
# waived all copyright and related or neighboring rights to this work.
# See: https://creativecommons.org/publicdomain/zero/1.0/

from itertools import ifilter, imap, islice

class Primes(object):
    def __init__(self):
        self.cache = []

    def __iter__(self):
        return self

    def next(self):
        if not self.cache:
            self.cache.append(2)
            return 2

        i = self.cache[-1]
        while True:
            i += 1
            if not any(imap(lambda p: 0 == i % p, self.cache)):
                self.cache.append(i)
                return i

primes = Primes()
for n in islice(ifilter(lambda p: 7777 == p % 10000, primes), 25):
    print(n)
